This page contains Frequently Asked Questions (FAQs) regarding Aurora Store's development as well as its features such as Device Spoofing, Aurora Services, Filters, etc.

## General

#### Why the name "Aurora" Store?

Initially, Aurora Store was named Galaxy Store. Some people thought we were trying to clickbait by using Samsung's "Galaxy" likeness (False, btw). Because of this we have decided to drop the Galaxy name in place of Aurora.

#### What is the difference between Aurora Store and Google's Play store?

Unlike Google's Play Store, Aurora Store doesn't track your downloads or the apps you use. We respect your privacy. Aurora Store is also unaffected by Google marking your device as _uncertified_ or lacking of necessary Google apps. Play Protect is not present, as this is a Play Store only feature.

#### Is Aurora Store a fork of YalpStore?

Technically, no. Aurora Store v3 is written from scratch, and Aurora Store v4 is a rewrite of v3 in Kotlin, but it does share some code from YalpStore.

#### Do I need Google Play Services to use Aurora Store?

No. Aurora Store was built to access the Google Play store without any kind of Google services. It doesn't matter if you use it with or without Google Play Services/MicroG.

#### What is the FakeStore?

Some poorly-designed apps detect if Google Play is missing and punish the user by misbehaving. The FakeStore is a stub that disguises itself as the Play store: FakeStore shares the same package name as the Play store `com.android.vending`. This prevents some apps from crashing.

#### Is it safe to use Aurora store?

Aurora Store is fully open-source and verified by F-Droid. If you're asking about the safety of the apps in the store, those are the exact same ones the Play Store would load and display. A lot of dangerous stuff seems to sneak past Google though, so as a rule of thumb, don't download anything which you're unsure about.

#### Why are some features from v3 not present in v4?

Similar to Aurora Store v3, version 4 of Aurora Store is rewritten from scratch, but now in Kotlin language. This means that each and every feature and UI elements needs to be implemented accordingly. With new features, bugs come popping up every now and then, bit & pieces to improve while keeping it as stable as possible. As you can see, this takes time and effort so please have some patience!

#### What data does Aurora Store send to Google?

Please read our [Privacy Policy](https://gitlab.com/AuroraOSS/AuroraStore/-/blob/master/POLICY.md)

#### What are nightly autobuilds?

> A \[...\] nightly build is the practice of completing a software build of the latest version of a program, on a daily basis. This is so it can first be compiled to ensure that all required dependencies are present \[...\]. The daily build is also often publicly available allowing access to the latest features for feedback.
> 
> ~ Source: [Wikipedia](https://en.wikipedia.org/wiki/Daily_build)

There are two versions of Aurora Store that we make available to all users.

- **Stable**: Stable versions with everything functioning, with minor to no bugs.
- **Nightly**: Daily versions built by a script to include the latest dependencies and commits on the Aurora Store repo. Can contain a few bugs and features the Stable version doesn't have.

You can download the latest nightlies via the website or with this [link](https://files.auroraoss.com/?folder=AuroraStore/Nightly).

#### "Please add support for F-Droid/Amazon/Yada repositories!"

No, this is a Play Store client only. Different clients for different services (^\_~)

#### Why do some apps show updates in Aurora Store (Anonymous mode) but not in the Play Store (or vice-versa)?

Aurora Store's anonymous mode works by connecting to a random dummy account stored in the token dispenser server. These dummy accounts are created by volunteers from different countries and some by the developer himself. Thus, every account has different locale settings by default according to the location where it was first created.

So next comes the Google Play Update roll-out mechanism. Google doesn't push app updates in one single attempt to all countries users. It's similar to staged updates & needs a lot of server syncing which takes time. Some apps are also blacklisted or restricted in specific countries and for specific devices (geolocked & device-restricted).

So while using Anonymous mode user are randomly connected to an Dummy account with different account locales which alas alters the list of app updates accordingly. Additionally spoof settings can also be able to alter the apps updates list.

#### How can I report a bug or suggest something?

You can open issues on GitLab [here](https://gitlab.com/AuroraOSS/AuroraStore/issues), or you can do so by joining our [Telegram Support Group](https://t.me/AuroraSupport). Use `/bug` or `/suggestion` at the start of your message.

## Accounts

#### Do I need to use my own Google account to log in?

Nope. Aurora Store can log you in with a dummy (anonymous) account that is stored in the token dispenser so that nothing gets linked to your own account.

However, it is recommended to use **your own Google account**, or at the very least a throwaway one which you will not have to worry about if it gets banned. This ensures better stability and gives you the full potential with the features available in Aurora Store.

#### What is the token dispenser?

The token dispenser is a server that provides login credentials to Aurora Store for Anonymous Logins. In case you keep getting the rate-limit error, check whether you are blocked at the `https://auroraoss.com/api/amiblocked` endpoint. If so, open a confidential issue with your IP address to unblock it.

If you would like to create and host your own token dispenser server, check out the source code [here](https://github.com/AuroraOSS/AuroraDispenser).

#### Why would I use my own account? Is it safe?

The main reason would be having the possibility to download apps purchased by yourself or to access your wishlist. Other reasons for using your own account could be having beta updates available, as this isn't possible for dummy accounts, or to reduce the possibility of having issues with Aurora Store when using dummy accounts.

However, you may want to be careful as **Google retains full rights to block any account under their** [**Google Play Terms of Service**](https://play.google.com/intl/en-us_us/about/play-terms/index.html), because using Aurora Store clearly violates their terms of services. Being banned means that the very Google account you used to sign in with will be blocked forever. It might be worth using a dummy account for that reason.

If you do happen to get your Google account banned, you can try appealing, which may or may not work. If they reject your appeal then there's nothing much you can do about that account. You can try your luck by filling out their form [here](https://support.google.com/accounts/contact/disabled2).

#### How do I purchase paid apps without using the Play Store app?

Purchase the apps from the [Google Play website](https://play.google.com/store), then log in using your own account in Aurora Store to download and install them.

#### Can Aurora store verify licenses?

That, unfortunately, is something tied to Play Store and probably always will be. If you don't want to install the Play Store (it could work with microG), all you can do is pester the Devs to remove the verification or at least offer alternative means of verification. In-App Purchases (IAPs) are similarly tied to Play Store and you will not be able to make or restore IAPs without having it installed.

#### Can I use Aurora Store to get paid apps for free?

Not unless they are on discount or made free.

## Filters

#### What is Aurora Store app only filter?

This filter, when enables list only those apps that were installed or updated by Aurora Store.

#### What is the F-Droid filter?

Since F-Droid signs APKs with its own keys, the Play store variants of apps cannot be installed over them. The F-Droid filter excludes all the apps it finds with F-Droid signatures or installer package as "org.fdroid.fdroid" or "org.fdroid.fdroid.privileged"  on your device to prevent such conflicts.

#### What is the Google filter?

By enabling this filter, you can remove all Google Apps from updates & installed app list.

## Downloads & Installation

#### Where can I find downloaded .apk files from Aurora Store?

Aurora Store utilizes the internal files directory located at `/data/data/com.aurora.store/files/Download`. This directory is inaccessible to other apps as part of Android's security model.

In case you are looking to share the installed apps, check the following apps:

- [LocalSend](https://localsend.org/) (Recommended): Share files to nearby devices. Free, open-source, cross-platform.

In case you want to backup the installed apps, check the following apps:

- [Seedvault](https://github.com/seedvault-app/seedvault) (Recommended): A backup application for the Android Open Source Project (FOSS & often pre-included in Custom ROMs)
- [Swift Backup](https://play.google.com/store/apps/details?id=org.swiftapps.swiftbackup): Simple, fast and smart backup solution for Android

If you still need access to the APK files downloaded by Aurora Store, navigate and turn off the `Settings > Installation > Delete APK post-install` setting. This will ensure that Aurora Store will not delete the APK files after installation. Next, head to the `Downloads` page and long-press a download to get the option to `Export app bundle` which will pack and allow you to export the downloaded files to a location of your choice.

> Downloaded files are automatically deleted after 6 hours regardless of the `Delete APK post-install` setting to preserve space on the device.

#### How does Aurora Store install apps?

Aurora Store can install apps in different ways (installers):

- **Session** (Recommended & Default): Session Installer uses the default app installation mechanism Android provides. This is the safest and recommended installer for installing apps as it doesn't require using a 3rd-party app or doing any kind of modifications to the OS.

Since 4.4.5, Session Installer optionally supports silently using Device Owner permissions to install apps without confirmation. This requires users to set Aurora Store as a device owner using the ADB command (no accounts must be present on the device).

```shell
adb shell dpm set-device-owner com.aurora.store/com.aurora.store.data.receiver.DeviceOwnerReceiver
```

- **Root** (Requires [Magisk](https://topjohnwu.github.io/Magisk/) or [KernelSU](https://kernelsu.org/)): Root Installer requires root permissions. It has the benefit of automatically installing apps in the background as soon as they are downloaded. Additionally, it mocks the installation source name to `com.android.vending` (Google Play Store).

- **Shizuku** (Requires [Shizuku](https://shizuku.rikka.app/)): Only supported on Android 8.0+ devices, Shizuku Installer uses the same technique as Session Installer. Using Shizuku allows us to bypass the hidden API restrictions imposed by the OS to allow background installation and mock the installation source name to `com.android.vending` (Google Play Store).

- **App Manager** (Requires [App Manager](https://muntashirakon.github.io/AppManager/en/)): The App Manager Installer uses App Manager to install the apps.

Apart from these installers, two additional choices were also supported in the past namely Native Installer and Aurora Services. However, both have been deprecated and users are advised to switch to a supported one.
    
#### Can Aurora download and install Split or Bundled APKs?

Yes, it can install both with or without root.

## Spoofing

#### Device

To spoof your device config, go to the **Spoofing** menu located at the sidebar and select your desired device config. Make sure to select one with the same architecture (e.g. arm64-v8a, armeabi-v7a, armeabi) and better yet, same Target Android Runtime APIs (API30: Android 11, API29: Android 10, API28: Android 9, etc.), otherwise you will experience problems with installing.

#### Language

Language spoofing allow you to change the content language displayed on Aurora Store. This does not mean the app language, rather the actual content like app titles, description, reviews & more. However, some strings are tied to your Google account if you are using your own account to log in, e.g. the Editor Choice section will be displayed in the language your account is set to (i.e. if your account country is set to Germany, the language displayed will be German).

#### Location

For now, location spoofing can only be done using own Google accounts. It's not possible to use anonymous accounts for location spoofing, as all sessions are generated via our server, so only our server's IP and the account locale will be shown to Google. Using a VPN is sufficient to spoof your location so that Aurora Store can show apps with the IP location you selected. Simply use a VPN client to connect to IP then open Aurora Store and login using Google account.

This is how Google determines the location of your account:

> An IP address (also called Internet address) is assigned to your device by your Internet Service Provider, and is a requirement to use the Internet. IP addresses are used to make the connection between your device and the websites and services that you use. IP addresses are roughly based on geography. This means that any website that you use, including google.com, may get some information about your general area.
> 
> ~ Source: [Google Policies](https://policies.google.com/technologies/location-data?hl=en-GB)