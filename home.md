Welcome to the Aurora Wiki!

If you're a newcomer or have questions, please read [FAQs](Frequently Asked Questions) before issuing problems on GitLab or asking for help in the Telegram support chat.

Other than that, indulge yourself with a good helping of docs!

## Table of Contents

- [Anonymous Logins](Anonymous Logins)
- [Troubleshooting](Troubleshooting)