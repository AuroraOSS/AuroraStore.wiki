Anonymous login feature is great for privacy enthusiasts however there are certain things you should know.

#### What is a Token Dispenser?

A token dispenser is a web client, that has around 250 real google accounts (inclusive of all servers), it distributes the AuthToken for anyone how makes a request to it in a defined format.

So basically all dispensers have the email-AASToken pairs on the server, when a user makes a request, the server logs in to google using available accounts, generates authentication token and this token is given to the user.

All the dispensers are configured to use dummy device profiles, default : Xiaomi Mi5.

(Why Xiaomi? Because I had this device when I first started android developement.)

#### What is an AuthToken?

In simple terms its a token that lets you access PlayStore service.

There are other complementary tokens required to make the api work, but that's done via Aurora Store and not related to Dispensers.

#### Why do Aurora Store always say "Session-expired"?

There are multiple things that may cause it:

- Multiple users are logged in using same dummy account provide by the dispenser, in different geographic locations.
- There is a limit (~12 - observed not real) for concurrent active login sessions, post that limit, Google invalidates old sessions.
- Google also has "rate-limit" policies on the usage of API, so if it detects overuse of the api, it just invalidates the authentication token.
    
#### Why am I not able to login anonymously \[OR\] I'm able to login on device A and not on device B ?

Again there can be multiple reasons:

- Network configuration, make sure Aurora Store has access to internet.
- Network filter, make sure you haven't blacklisted "Cloud Flare" all the dispenser are behind Cloud Flare, so if its DNS is blocked, you cant reach dispensers.
- Tor connections \[OR\] Orbot setup, again Cloud Flare has some weird issues with TOR setup, I'm looking into it.

### How to create an Account?

- Head to the Google Signup Page [here](https://accounts.google.com/signup)
- Enter Details
   - You may choose to fill in random form values
   - Be nice & DO NOT fill offensive data
   - Choose a strong password, try [Genratr](https://www.genratr.com)
- Do not fill in Phone numbers & backup emails
   - If a phone number is mandatory, change network or use a VPN
   - If backup email is mandatory, fill in `aurora.oss.backup@gmail.com`

### How to generate AAS Token?

- Download & Install Authenticator App from [here](https://github.com/whyorean/Authenticator/releases/download/1.0.2/Authenticator_v1.0.2.apk)
   - Another opensource app by Aurora OSS Dev, [source](https://github.com/whyorean/Authenticator)
- Open the App & Fill the email & password for the newly created account
   - The app will ask you to accept the Google TOS 
- Once TOS is accepted, it will generate an AAS token for you

### Want to share an account with Aurora Account Pool?

- Create a new account
- Generate the AAS Token
- Share the Email + AAS Token (no password required) with the Aurora OSS Team
   - Share it directly to dev either via [Telegram](https://t.me/whyorean) or [E-mail](mailto:rahul@auroraoss.com)
   - Share it with team (admins & mods) [here](https://t.me/AuroraSupport)
